<?php


namespace App\Services\External;


use App\Services\External\Shimotomo\ShimotomoApiErrorException;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;
use Illuminate\Http\Response;
use RuntimeException;

class ShimotomoService
{
    const API_URL = 'https://preprod.shimotomo.com';
    private $version = 'v1';
    private $requestClient;
    private $authToken;

    /**
     * ShimotomoService constructor.
     * @param ClientInterface $client
     */
    public function __construct(ClientInterface $client)
    {
        $this->requestClient = $client;
    }

    /**
     * @param $merchantTransactionId
     * @param $amount
     * @return string
     */
    public static function generateMD5Checksum($merchantTransactionId, $amount)
    {
        $values = config('services.shimotomo.merchantId') . "|" .
            config('services.shimotomo.merchantSecret') . "|" .
            $merchantTransactionId . "|" .
            $amount;

        return md5($values);
    }

    /**
     * @param string $version
     */
    public function version(string $version)
    {
        $this->version = $version;
    }

    /**
     * @return mixed
     * @throws ShimotomoApiErrorException
     */
    public function auth()
    {
        $params = [
            'authentication.partnerId' => config('services.shimotomo.partnerId'),
            'merchant.username' => config('services.shimotomo.merchantUsername'),
            'authentication.sKey' => config('services.shimotomo.merchantSecret'),
        ];

        $responseJson = $this->makeRequest('POST', $this->getAuthenticationUrl(), $params);
        $this->authToken = $responseJson->AuthToken;
        return $this;
    }

    /**
     * @throws ShimotomoApiErrorException
     */
    public function getAuthToken()
    {
        if (!$this->authToken) {
            $this->auth();
        }

        return $this->authToken;
    }
    /**
     * @param array $params
     * @return mixed
     * @throws ShimotomoApiErrorException
     */
    public function synchronous(array $params)
    {
        $headers = [
            'AuthToken' => $this->getAuthToken(),
        ];

        return $this->makeRequest('POST', $this->getSynchronousUrl(), $params, $headers);
    }

    /**
     * @param $httpVerb
     * @param $url
     * @param $arguments
     * @param array $headers
     * @return mixed
     * @throws ShimotomoApiErrorException
     */
    private function makeRequest($httpVerb, $url, $arguments, $headers = [])
    {
        try {
            $response = $this->requestClient->request($httpVerb, $url, [
                RequestOptions::HEADERS => $headers,
                RequestOptions::JSON => $arguments
            ]);
            $responseJson = json_decode($response->getBody()->getContents());
        } catch (GuzzleException $exception) {
            throw new ShimotomoApiErrorException($exception->getMessage());
        } catch (RuntimeException $exception) {
            throw new ShimotomoApiErrorException($exception->getMessage());
        }

        if ($response->getStatusCode() !== Response::HTTP_OK) {
            throw new ShimotomoApiErrorException();
        }

        return $responseJson;
    }

    /**
     * @return string
     */
    private function getSynchronousUrl()
    {
        return self::API_URL . '/transactionServices/REST/' . $this->version . '/payments';
    }

    /**
     * @return string
     */
    private function getAuthenticationUrl()
    {
        return self::API_URL . '/transactionServices/REST/' . $this->version . '/authToken';
    }
}
